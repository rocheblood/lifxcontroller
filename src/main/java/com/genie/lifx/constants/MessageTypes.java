package com.genie.lifx.constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.exception.LIFXException;
import com.genie.lifx.model.LIFXMessage;
import com.genie.lifx.model.LIFXPayload;
import com.genie.lifx.model.payloads.Get101;
import com.genie.lifx.model.payloads.SetColor102;
import com.genie.lifx.model.payloads.SetPower117;
import com.genie.lifx.model.payloads.State107;
import com.genie.lifx.model.strategies.LIFXMessageStrategy;

/**
 * Convenience enum for each message type. Returns a base msg with the default payload.
 * 
 * @author Matthew Roche
 *
 */
public enum MessageTypes {

	/**
	 * To device. Triggers a {@link #STATE_107} message.
	 */
	GET_101(101, Get101.class, new LIFXMessageStrategy()
				.setResRequired(true)),
	/**
	 * To device
	 */
	SET_COLOR_102(102, SetColor102.class, new LIFXMessageStrategy()),
	
	/**
	 * From device. Response to GET_101
	 */
	STATE_107(107, State107.class, new LIFXMessageStrategy()),
	
	/**
	 * To device
	 */
	SET_POWER_117(117, SetPower117.class, new LIFXMessageStrategy()),
	;
	
	private Logger logger = LoggerFactory.getLogger(MessageTypes.class);
	
	private int type;
	private LIFXMessageStrategy strategy;
	private Class<? extends LIFXPayload> payload;
	
	private <T extends LIFXPayload> MessageTypes(int value, Class<T> payload, LIFXMessageStrategy strategy) {
		this.type = value;
		this.payload = payload;
		this.strategy = strategy;
	}
	
	public int getType() {
		return this.type;
	}
	
	/**
	 * Returns the builder object that includes the default payload for this message
	 * @return
	 */
	public LIFXMessage.Builder getBuilder() {
		return new LIFXMessage.Builder(this.strategy).payload(getPayload());
	}
	
	public LIFXMessage.Builder getBuilder(LIFXPayload payload) {
		return new LIFXMessage.Builder(this.strategy).payload(payload);
	}
	
	/**
	 * Returns the message including the payload for this type
	 * @return
	 */
	public LIFXMessage getMessage() {
		return new LIFXMessage.Builder(this.strategy).payload(getPayload()).build();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends LIFXPayload> T getPayload() {
		try {
			return (T) payload.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			String errMsg = String.format("Failed to instantiate payload for [%1$s]", this.name());
			logger.error(errMsg, e);
			throw new LIFXException(errMsg, e);
		}
	}
	
	/**
	 * Given a message type (LIFXMessage::LIFXHeader::LIFXProtocolHeader::Type), 
	 * return a message template
	 * 
	 * @param type
	 * @return
	 */
	public static MessageTypes getMessageTypesFromType(int type) {
		for (MessageTypes mt : MessageTypes.values()) {
			if (mt.getType() == type) {
				return mt;
			}
		}
		throw new IllegalArgumentException(String.format("Not MessageTypes object for type [%1$s]", type));
	}
}
