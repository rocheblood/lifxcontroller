package com.genie.lifx.constants;

import com.genie.lifx.util.PropertiesManager;
import com.genie.lifx.util.PropertiesManager.PropertyConverter;

public enum Properties {
	
	/**
	 * Time is milliseconds to wait for a response in a blocking call.
	 */
	RESPONSE_TIMEOUT("com.genie.lifx.response.timeout", 5000L, Long.class),
	
	/**
	 * Time in which a response will remain in the response queue before 
	 * being purged. When a response is collected by the caller, it is removed from the queue.
	 * 
	 * Default is 5 minutes
	 */
	RESPONSE_QUEUE_LIFESPAN("com.genie.lifx.response.queue.lifespan", 300000L, Long.class),
	
	/**
	 * Port to contact LIFX devices on and listen for responses.
	 */
	DEVICE_PORT("com.genie.lifx.port", 56700, Integer.class),
	
	/**
	 * Address to broadcast a message on the local network.
	 */
	BROADCAST_ADDRESS("com.genie.lifx.broadcast.address", "255.255.255.255", String.class),
	
	/**
	 * When broadcasting, multiple responses may be expected. This is the 
	 * waiting time for each broadcast that expects responses.
	 * 
	 */
	BROADCAST_RESPONSE_DELAY("com.genie.lifx.broadcast.response.delay", 500L, Long.class),
	
	/**
	 * How often to execute the cleaning thread to purge expired responses. In minutes
	 */
	CLEANER_FREQUENCY("com.genie.lifx.cleaner.frequency", 10L, Long.class);
	
	private String property;
	private Object defaultValue;
	private Object value;
	private Class<?> type;
	
	Properties(String property, Object defaultValue, Class<?> type) {
		PropertiesManager pm = PropertiesManager.getInstance();
		this.property = property;
		this.type = type;
		this.defaultValue = defaultValue;
		
		//This converter ensures that type casting in the generic getValue works correctly
		PropertyConverter<?> converter = pm.getPropertyConverter(type);
		this.value = converter.get(pm.get(property, defaultValue));
	}

	public String getProperty() {
		return property;
	}

	@SuppressWarnings("unchecked")
	public <T> T getDefaultValue() {
		return (T) defaultValue.getClass().cast(defaultValue);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getValue() {
		return (T) value.getClass().cast(value);
	}

	public Class<?> getType() {
		return type;
	}
}
