package com.genie.lifx.constants;

public enum PowerLevels {
	ON(0xFFFF),
	OFF(0x0000);
	
	private short value;
	
	PowerLevels(int value) {
		this.value = (short) value;
	}
	
	public short getValue() {
		return value;
	}
}
