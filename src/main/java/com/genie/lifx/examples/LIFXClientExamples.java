package com.genie.lifx.examples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.constants.MessageTypes;
import com.genie.lifx.constants.PowerLevels;
import com.genie.lifx.model.LIFXMessage;
import com.genie.lifx.model.payloads.SetColor102;
import com.genie.lifx.model.payloads.SetPower117;
import com.genie.lifx.model.payloads.State107;
import com.genie.lifx.model.structures.HSBKColor;
import com.genie.lifx.network.LIFXClient;
import com.genie.lifx.network.model.LIFXClientResponse;

public class LIFXClientExamples {

	private static Logger logger = LoggerFactory.getLogger(LIFXClientExamples.class);

	private static String ip = "192.168.1.242";

	public static void main(String[] args) throws InterruptedException {
		LIFXClient client = LIFXClient.getInstance();
		try {
			client.open();
//			setPower(client);
//			getState(client);
			setColour(client);
		} finally {
			logger.debug("closing client");
			client.close();
		}
	}

	public static void getState(LIFXClient client) {
		LIFXMessage request = MessageTypes.GET_101.getMessage();

		LIFXClientResponse response = client.sendForResponse(request);
		logger.debug("Response received with ID [{}] and size [{}]", response.getId(), response.size());
		for (LIFXMessage msg : response.getMessages()) {
			State107 respPayload = msg.getPayload();
			logger.debug("Label is : [{}], power is on [{}]", respPayload.getLabel(), respPayload.isOn());
		}
	}

	public static void setPower(LIFXClient client) throws InterruptedException {
		SetPower117 payload = MessageTypes.SET_POWER_117.getPayload();
		payload.setLevel(PowerLevels.OFF);
		LIFXMessage msg = MessageTypes.SET_POWER_117.getBuilder(payload)
//				.ip(ip)
				.build();
		client.send(msg);
	}

	public static void setColour(LIFXClient client) throws InterruptedException {
		SetColor102 payload = MessageTypes.SET_COLOR_102.getPayload();
		payload.setColor(HSBKColor.WARM_WHITE.setBrightness(1.0f));
		LIFXMessage msg = MessageTypes.SET_COLOR_102.getBuilder(payload)
//				.ip(ip)
				.build();
		logger.debug(msg.getHexData());
		client.send(msg);
	}
}
