package com.genie.lifx.exception;

public class LIFXException extends RuntimeException {

	private static final long serialVersionUID = -8066119451855522834L;

	public LIFXException(String msg) {
		super(msg);
	}
	
	public LIFXException(String msg, Object...msgArgs) {
		super(String.format(msg, msgArgs));
	}
	
	public LIFXException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public LIFXException(String msg, Throwable t, Object... msgArgs) {
		super(String.format(msg, msgArgs), t);
	}
}
