package com.genie.lifx.exception;

public class LIFXValidationException extends LIFXException {

	private static final long serialVersionUID = -8253501784415632983L;
	
	private static String template = "Validation error. Error is [%1$s]";

	public LIFXValidationException(String errorMsg) {
		super(String.format(template, errorMsg));
	}
	
}
