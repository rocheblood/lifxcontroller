package com.genie.lifx.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractLIFXComponent implements LIFXComponent {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected final String name = this.getClass().getSimpleName();

	@Override
	public void setFromBytes(String hex) {
		if (hex.length() / 2 != getByteCount()) {
			String errMsg = String.format("Could not create component [%1$s] from hex [%2$s]. Unexpected byte size [%3$s/%4$s].", 
					name,
					hex,
					hex.length() / 2,
					getByteCount());
			throw new IllegalArgumentException(errMsg);
		}
		logger.trace("Creating [{}] from hex string [{}]", name, hex);
	}
	
}
