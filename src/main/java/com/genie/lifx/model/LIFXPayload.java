package com.genie.lifx.model;

/**
 * Parent class for all LIFX message payloads. Each implementation
 * should have a default constructor to initiate sensible defaults
 * @author Matthew Roche
 *
 */
public abstract class LIFXPayload extends AbstractLIFXComponent {

	private final int type;
	
	public LIFXPayload() {
		this(0);
	}
	
	public LIFXPayload(final int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}
}
