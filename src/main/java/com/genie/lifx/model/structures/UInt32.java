package com.genie.lifx.model.structures;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.genie.lifx.util.ByteUtil;

public class UInt32 extends AbstractUInt {
	
	private static final int SIZE = 4;
	
	public UInt32() {}
	
	public UInt32(byte[] value) {
		setData(value);
	}
	
	public UInt32(int value) {
		data = ByteBuffer.allocate(SIZE).putInt(value).array();
	}

	public int getByteCount() {
		return SIZE;
	}

	public String getHexData(ByteOrder order) {
		return ByteUtil.getHex(ByteBuffer.wrap(data).getInt(), order);
	}
}
