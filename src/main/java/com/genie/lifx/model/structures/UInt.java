package com.genie.lifx.model.structures;

import java.nio.ByteOrder;

public interface UInt {
	
	int getByteCount();
	
	/**
	 * Get hex data in default order (Little Endian)
	 * @return
	 */
	String getHexData();
	
	/**
	 * Get hex data in selected order
	 * @param order
	 * @return
	 */
	String getHexData(ByteOrder order);
	
	/**
	 * Allows individual bit adjustment. Valid values are 1 or 0. Some examples
	 * <ul>
	 * <li>Position 0, value = 1. Would set the value for the most significant bit in the most significant byte. 0x[1]0000000 00000000</li>
	 * <li>Position 8, value = 1. Would set the value for the most significant bit in the second most significant byte. 0x00000000 [1]0000000</li>
	 * <li>Position 9, value = 1. Would set the value for the second most significant bit in the second most significant byte. 0x00000000 0[1]000000</li>
	 * </ul>
	 * @param position
	 * @param value can be 1 or 0
	 */
	void setBitAt(int position, int value);
	
	/**
	 * Retrieves the bit value at a given position starting from the most significant bit
	 * <ul>
	 * <li>Position 0, 0x[1]001 0000. Would return 1</li>
	 * <li>Position 1, 0x1[0]01 0000. Would return 0</li>
	 * <li>Position 3, 0x100[1] 0000. Would return 1</li>
	 * <li>Position 7, 0x1001 000[0]. Would return 0</li>
	 * </ul> 
	 * @param position
	 * @return
	 */
	byte getBitAt(int position);
}
