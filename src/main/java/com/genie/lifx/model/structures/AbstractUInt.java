package com.genie.lifx.model.structures;

import java.nio.ByteOrder;
import java.util.Arrays;

public abstract class AbstractUInt implements UInt {
	
	protected byte[] data = new byte[getByteCount()];
	
	protected final String name = this.getClass().getSimpleName();
	
	/**
	 * Max index of a bit
	 */
	private int maxPos = (data.length * 8) - 1;

	@Override
	public void setBitAt(int position, int value) {
		if (position < 0 || position > maxPos) {
			String message = String.format("Position out of bounds. Position : [%1$s], Data length [%2$s]", position, maxPos);
			throw new IllegalArgumentException(message);
		}
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException("Bit value must be 1 or 0.");
		}
		int byteNum = position / 8;
		int bitNum = position % 8;
		if (value == 1) {
			data[byteNum] |= 0x80 >> bitNum; //0x1000 0000
		} else {
			data[byteNum] &= ~(0x80 >> bitNum);
		}
	}
	
	@Override
	public byte getBitAt(int position) {
		if (position < 0 || position > maxPos) {
			String message = String.format("Position out of bounds. Position : [%1$s], Data length [%2$s]", position, maxPos);
			throw new IllegalArgumentException(message);
		}
		int byteNum = position / 8;
		int bitNum = position % 8;
		byte value = (byte) ((data[byteNum] >> (7 - bitNum)) & 1); //Allows ordering so pos 0 is the most significant bit
		return value;
	}
	
	public byte[] getData() {
		return data;
	}
	
	public void setData(byte[] data) {
		if (data.length != getByteCount()) {
			String message = String.format("%1$s data must contain [%2$s] bytes", name, getByteCount());
			throw new IllegalArgumentException(message);
		}
		this.data = data;
	}
	
	@Override
	public String getHexData() {
		return getHexData(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + maxPos;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractUInt other = (AbstractUInt) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		if (maxPos != other.maxPos)
			return false;
		return true;
	}

	
}
