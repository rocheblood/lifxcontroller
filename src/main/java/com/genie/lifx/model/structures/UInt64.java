package com.genie.lifx.model.structures;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.genie.lifx.util.ByteUtil;

public class UInt64 extends AbstractUInt {
	
	private static final int SIZE = 8;
	
	public UInt64() {}
	
	public UInt64(byte[] value) {
		setData(value);
	}
	
	public UInt64(long value) {
		data = ByteBuffer.allocate(SIZE).putLong(value).array();
	}

	public int getByteCount() {
		return SIZE;
	}

	public String getHexData(ByteOrder order) {
		return ByteUtil.getHex(ByteBuffer.wrap(data).getLong(), order);
	}
}
