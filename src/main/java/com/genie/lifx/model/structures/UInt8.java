package com.genie.lifx.model.structures;

import java.nio.ByteOrder;

import com.genie.lifx.util.ByteUtil;

public class UInt8 extends AbstractUInt {
	
	private static final int SIZE = 1;
	
	public UInt8() {}
	
	public UInt8(byte[] value) {
		setData(value);
	}
	
	public UInt8(int value) {
        short valueShort = (short) value;
        data[0] = (byte) (valueShort & 0xff);
	}

	public int getByteCount() {
		return SIZE;
	}

	public String getHexData(ByteOrder order) {
		return ByteUtil.getHex(data);
	}
}
