package com.genie.lifx.model.structures;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.genie.lifx.model.AbstractLIFXComponent;

public class HSBKColor extends AbstractLIFXComponent {
	
	/**
	 * Provides an empty HSBK colour object
	 */
	public static final HSBKColor NONE = new HSBKColor();
	public static final HSBKColor WARM_WHITE = new HSBKColor(0, 0.0f, 1.0f, 3500);
	public static final HSBKColor BLUE_WHITE = new HSBKColor(0, 0.0f, 1.0f, 8000);
	public static final HSBKColor RED = new HSBKColor(360, 0.9f, 1.0f, 3500);
	public static final HSBKColor BLUE = new HSBKColor(230, 0.8f, 1.0f, 3500);
	public static final HSBKColor GREEN = new HSBKColor(120, 0.85f, 1.0f, 3500);
	public static final HSBKColor YELLOW = new HSBKColor(65, 0.85f, 1.0f, 3500);

	public static final int MIN_KELVIN = 2500;
	public static final int MAX_KELVIN = 9000;

	private UInt16 hue;
	private UInt16 saturation;
	private UInt16 brightness;
	private UInt16 kelvin;
	
	private HSBKColor() {
		hue = new UInt16();
		saturation = new UInt16();
		brightness = new UInt16();
		kelvin = new UInt16();
	}
	
	/**
	 * @param hue [0, 360]
	 * @param saturation [0, 1]
	 * @param brightness [0, 1]
	 * @param kelvin [2500, 9000]
	 */
	public HSBKColor(float hue, float saturation, float brightness, int kelvin) {
		if(hue < 0 || hue > 360) {
            throw new IllegalArgumentException("Hue must be between 0 and 360");
        }
        if(saturation < 0 || saturation > 1) {
            throw new IllegalArgumentException("Saturation must be between 0 and 1");
        }
        if(brightness < 0 || brightness > 1) {
            throw new IllegalArgumentException("Brightness must be between 0 and 1");
        }
        if(kelvin < MIN_KELVIN || kelvin > MAX_KELVIN) {
        		String errMsg = String.format("Kelvin must be between [%1$s] and [%2$s]", MIN_KELVIN, MAX_KELVIN);
            throw new IllegalArgumentException(errMsg);
        }
        short sHue = (short) ((hue / 360) * 65535);
        short sSat = (short) ((saturation / 1.00f) * 65535);
        short sBright = (short) ((brightness / 1.00f) * 65535);
        
        this.hue = new UInt16(sHue);
        this.saturation = new UInt16(sSat);
        this.brightness = new UInt16(sBright);
        this.kelvin = new UInt16((short) kelvin);
	}

	public UInt16 getHue() {
		return hue;
	}

	public UInt16 getSaturation() {
		return saturation;
	}
	
	public HSBKColor setBrightness(float brightness) {
		if(brightness < 0 || brightness > 1) {
            throw new IllegalArgumentException("Brightness must be between 0 and 1");
        }
		short sBright = (short) ((brightness / 1.00f) * 65535);
		this.brightness = new UInt16(sBright);
		return this;
	}

	public UInt16 getBrightness() {
		return brightness;
	}

	public UInt16 getKelvin() {
		return kelvin;
	}

	@Override
	public int getByteCount() {
		return hue.getByteCount() + saturation.getByteCount() + brightness.getByteCount() + kelvin.getByteCount();
	}

	@Override
	public String getHexData() {
		return hue.getHexData() + saturation.getHexData() + brightness.getHexData() + kelvin.getHexData();
	}

	@Override
	public String print() {
		JsonObjectBuilder builder = Json.createObjectBuilder()
				.add("hue", hue.getHexData())
				.add("sat", saturation.getHexData())
				.add("bri", brightness.getHexData())
				.add("kel", kelvin.getHexData());
		String json = builder.build().toString();
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		short sHue = new BigInteger(hex.substring(0, 4), 16).shortValue();
		short sSat = new BigInteger(hex.substring(4, 8), 16).shortValue();
		short sBright = new BigInteger(hex.substring(8, 12), 16).shortValue();
		short sKel = new BigInteger(hex.substring(12, 16), 16).shortValue();
		
		hue.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sHue).array());
		saturation.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sSat).array());
		brightness.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sBright).array());
		kelvin.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sKel).array());
	}
}
