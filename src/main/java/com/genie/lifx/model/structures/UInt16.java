package com.genie.lifx.model.structures;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.genie.lifx.util.ByteUtil;

public class UInt16 extends AbstractUInt {
	
	private static final int SIZE = 2;
	
	public UInt16() {}
	
	public UInt16(byte[] value) {
		setData(value);
	}
	
	public UInt16(int value) {
        data = ByteBuffer.allocate(SIZE).putShort((short) value).array();
	}
	
	public int getByteCount() {
		return SIZE;
	}

	public String getHexData(ByteOrder order) {
		return ByteUtil.getHex(ByteBuffer.wrap(data).getShort(), order);
	}
}
