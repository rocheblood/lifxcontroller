package com.genie.lifx.model;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;

import com.genie.lifx.model.structures.UInt16;
import com.genie.lifx.model.structures.UInt64;

public class LIFXProtocolHeader extends AbstractLIFXComponent {
	
	private UInt64 reserved1 = new UInt64();
	
	private UInt16 type = new UInt16();
	
	private UInt16 reserved2 = new UInt16();
	
	public void setType(short type) {
		this.type.setData(ByteBuffer.allocate(2).putShort(type).array());
	}
	
	public short getType() {
		short value = ByteBuffer.wrap(type.getData()).getShort();
		return value;
	}
	
	@Override
	public int getByteCount() {
		return reserved1.getByteCount()
				+ type.getByteCount()
				+ reserved2.getByteCount();
	}

	@Override
	public String getHexData() {
		String hex = reserved1.getHexData()
				+ type.getHexData()
				+ reserved2.getHexData(); 
		logger.trace("[{}] hex data [{}]", name, hex);
		return hex;
	}

	@Override
	public String print() {
		short sType = ByteBuffer.wrap(type.getData()).getShort();
		String json = Json.createObjectBuilder()
				.add("type", sType)
				.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		//No need to set reserved fields
		short sType = new BigInteger(hex.substring(16, 20), 16).shortValue();
		type.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sType).array());
	}
	
}

