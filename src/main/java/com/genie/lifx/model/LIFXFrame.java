package com.genie.lifx.model;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;

import com.genie.lifx.model.structures.UInt16;
import com.genie.lifx.model.structures.UInt32;

public class LIFXFrame extends AbstractLIFXComponent {

	/**
	 * Contains the byte size of the whole message, including size.
	 */
	private UInt16 size = new UInt16();

	/**
	 * Contains 
	 * <ul>
	 * <li>origin : 2 bits (0x00)</li>
	 * <li>tagged : 1 bit (default off)</li>
	 * <li>addressable: 1 bit (must be true)</li>
	 * <li>protocol : 12 bit (must be dec 1024)</li>
	 * </ul>
	 */
	private UInt16 mixedBytes = new UInt16();

	/**
	 * Source identifier: unique value set by the client, used by responses
	 */
	private UInt32 source = new UInt32();

	public LIFXFrame() {
		// origin/tagged/addressable should appear in the first byte
		short protocolNum = 1024;
		short val = (short) (0x00 << 8 | 0x00 << 8 | 0x10 << 8 | protocolNum);
		mixedBytes = new UInt16(val);
	}
	
	public void setSize(int size) {
		this.size = new UInt16(size);
	}
	
	public void setTagged(boolean tagged) {
		mixedBytes.setBitAt(2, tagged ? 1 : 0);
	}
	
	public boolean isTagged() {
		return mixedBytes.getBitAt(2) == 1;
	}
	
	public void setSource(int value) {
		byte[] data = ByteBuffer.allocate(4).putInt(value).array();
		source.setData(data);
	}
	
	public int getSource() {
		return ByteBuffer.wrap(source.getData()).getInt();
	}

	@Override
	public int getByteCount() {
		return size.getByteCount()
				+ mixedBytes.getByteCount()
				+ source.getByteCount();
	}

	@Override
	public String getHexData() {
		String hex = size.getHexData()
				+ mixedBytes.getHexData()
				+ source.getHexData();
		logger.trace("[{}] hex data [{}]", name, hex);
		return hex;
	}

	@Override
	public String print() {
		short sSize = ByteBuffer.wrap(size.getData()).getShort();
		String json = Json.createObjectBuilder()
				.add("size", sSize + " bytes")
				.add("tagged", isTagged())
				.add("source", source.getHexData())
				.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		//size, mixedBytes, source
		short sSize = new BigInteger(hex.substring(0, 4), 16).shortValue();
		short sMixedBytes = new BigInteger(hex.substring(4, 8), 16).shortValue();
		int iSource = new BigInteger(hex.substring(8, 16), 16).intValue();
		
		size.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sSize).array());
		mixedBytes.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sMixedBytes).array());
		source.setData(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(iSource).array());
	}
}
