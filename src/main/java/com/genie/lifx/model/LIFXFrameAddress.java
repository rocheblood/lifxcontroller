package com.genie.lifx.model;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;

import com.genie.lifx.model.structures.UInt64;
import com.genie.lifx.model.structures.UInt8;
import com.genie.lifx.util.ByteUtil;

public class LIFXFrameAddress extends AbstractLIFXComponent {
	
	/**
	 * 6 byte device address (MAC address) or zero (0) means all devices.
	 * Final two bytes are 0 padded
	 */
	private UInt64 target = new UInt64();
	
	/**
	 * Reserved. All 0
	 */
	private byte[] reserved1 = new byte[6];
	
	/**
	 * Combined byte made up of
	 * <ul>
	 * <li>Reserved - first 6 bit</li>
	 * <li>ack_required - 1 bit. Acknowledgement message required</li>
	 * <li>res_required - 1 bit. Response message required</li>
	 * </ul>
	 * <p>
	 * 	res_required is defaulted to true, ack_required is defaulted to false
	 * </p>
	 */
	private UInt8 resAckRes = new UInt8(0x01);
	
	/**
	 * Wrap around message sequence number. Used as a request identifier
	 */
	private UInt8 sequence = new UInt8();
	
	/**
	 * The input will be shifted 2 bytes left. Expectation is a 6 byte MAC address
	 * @param target
	 */
	public void setTarget(long target) {
		long value = target << 16;
		this.target.setData(ByteBuffer.allocate(8).putLong(value).array());
	}
	
	public long getTarget() {
		long value = ByteBuffer.wrap(target.getData()).getLong();
		return value;
	}
	
	/**
	 * Formats the target into a colon separated hex string
	 * @return
	 */
	public String getMAC() {
		return target.getHexData(ByteOrder.BIG_ENDIAN).substring(0, 12).replaceAll("(.{2})", "$1:");
	}
	
	public void setAcknowledgementRequired(boolean acknowledgementRequired) {
		resAckRes.setBitAt(6, acknowledgementRequired ? 1 : 0);
	}
	
	public boolean isAcknowledgementRequired() {
		return resAckRes.getBitAt(6) == 1;
	}
	
	public void setResponseRequired(boolean responseRequired) {
		resAckRes.setBitAt(7, responseRequired ? 1 : 0);
	}
	
	public boolean isResponseRequired() {
		return resAckRes.getBitAt(7) == 1;
	}
	
	public void setSequence(byte sequence) {
		this.sequence.setData(new byte[] {sequence} );
	}
	
	public boolean isSequenceSet() {
		byte val = this.sequence.getData()[0];
		return val != 0x0; 
	}
	
	public byte getSequence() {
		return this.sequence.getData()[0];
	}

	@Override
	public int getByteCount() {
		return target.getByteCount() +
				reserved1.length +
				resAckRes.getByteCount() +
				sequence.getByteCount();
	}

	@Override
	public String getHexData() {
		String hex = target.getHexData(ByteOrder.BIG_ENDIAN) //Target is left justified and left shifted
				+ ByteUtil.getHex(reserved1)
				+ resAckRes.getHexData()
				+ sequence.getHexData(); 
		logger.trace("[{}] hex data [{}]", name, hex);
		return hex;
	}

	@Override
	public String print() {
		String MAC = target.getHexData(ByteOrder.BIG_ENDIAN).substring(0, 12).replaceAll("(.{2})", "$1:");
		String json = Json.createObjectBuilder()
				.add("target", MAC)
				.add("acknowledgementRequired", isAcknowledgementRequired())
				.add("responseRequired", isResponseRequired())
				.add("sequence", sequence.getHexData())
				.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
 		long lTarget = new BigInteger(hex.substring(0, 16), 16).longValue();
		//16 to 28 reserved
		byte bResAckRes = new BigInteger(hex.substring(28, 30), 16).byteValue();
		byte bSequence = new BigInteger(hex.substring(30, 32), 16).byteValue();
		
		target.setData(ByteBuffer.allocate(8).putLong(lTarget).array());
		resAckRes.setData(ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(bResAckRes).array());
		sequence.setData(ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(bSequence).array());
	}
}

