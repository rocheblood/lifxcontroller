package com.genie.lifx.model.strategies;

import static com.genie.lifx.constants.Properties.BROADCAST_ADDRESS;
import static com.genie.lifx.constants.Properties.DEVICE_PORT;

import com.genie.lifx.model.LIFXPayload;

public class LIFXMessageStrategy implements Strategy {

	private String ip = BROADCAST_ADDRESS.getValue();
	private Integer port = DEVICE_PORT.getValue();
	
	private boolean tagged = false;
	private Integer source;
	private Long target;
	private boolean ackRequired = false;
	private boolean resRequired = false;
	private Integer sequence;
	private Integer type;
	
	private LIFXPayload payload;

	public String getIp() {
		return ip;
	}

	public LIFXMessageStrategy setIp(String ip) {
		this.ip = ip;
		return this;
	}
	
	public LIFXMessageStrategy broadcast() {
		this.ip = BROADCAST_ADDRESS.getValue();
		return this;
	}

	public Integer getPort() {
		return port;
	}

	public LIFXMessageStrategy setPort(Integer port) {
		this.port = port;
		return this;
	}

	public boolean isTagged() {
		return tagged;
	}

	public LIFXMessageStrategy setTagged(boolean tagged) {
		this.tagged = tagged;
		return this;
	}

	public Integer getSource() {
		return source;
	}

	public LIFXMessageStrategy setSource(Integer source) {
		this.source = source;
		return this;
	}

	public Long getTarget() {
		return target;
	}

	public LIFXMessageStrategy setTarget(Long target) {
		this.target = target;
		return this;
	}

	public boolean isAckRequired() {
		return ackRequired;
	}

	public LIFXMessageStrategy setAckRequired(boolean ackRequired) {
		this.ackRequired = ackRequired;
		return this;
	}

	public boolean isResRequired() {
		return resRequired;
	}

	public LIFXMessageStrategy setResRequired(boolean resRequired) {
		this.resRequired = resRequired;
		return this;
	}

	public Integer getSequence() {
		return sequence;
	}

	public LIFXMessageStrategy setSequence(Integer sequence) {
		this.sequence = sequence;
		return this;
	}

	public Integer getType() {
		return type;
	}

	public LIFXMessageStrategy setType(Integer type) {
		this.type = type;
		return this;
	}

	public LIFXPayload getPayload() {
		return payload;
	}

	public LIFXMessageStrategy setPayload(LIFXPayload payload) {
		this.payload = payload;
		return this;
	}
}
