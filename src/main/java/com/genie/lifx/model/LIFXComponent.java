package com.genie.lifx.model;

public interface LIFXComponent {

	/**
	 * Used to create a LIFXComponent from a hex string
	 * @param hex
	 */
	void setFromBytes(String hex);

	/**
	 * Return the number of bytes present in this component
	 * @return
	 */
	int getByteCount();
	
	/**
	 * Returns the finalised hex representation of this component.
	 * @return
	 */
	String getHexData();
	
	/**
	 * Returns a string that is a readable representation of this component 
	 * @return
	 */
	String print();
}
