package com.genie.lifx.model.payloads;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.genie.lifx.constants.PowerLevels;
import com.genie.lifx.model.LIFXPayload;
import com.genie.lifx.model.structures.HSBKColor;
import com.genie.lifx.model.structures.UInt16;
import com.genie.lifx.model.structures.UInt64;

/**
 * Sent by a device to provide the current light state.
 * @author Matthew Roche
 *
 */
public class State107 extends LIFXPayload {
	
	private HSBKColor colour = HSBKColor.NONE;
	private UInt16 reserved1 = new UInt16();
	private UInt16 power = new UInt16();
	
	/**
	 * 32 byte String
	 */
	private String label;
	private UInt64 reserved2 = new UInt64();
	
	public State107() {
		super(107);
	}
	
	public UInt16 getPower() {
		return this.power;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public String getColour() {
		return this.colour.getHexData();
	}
	
	public boolean isOn() {
		short state = ByteBuffer.wrap(power.getData()).getShort();
		return state == PowerLevels.ON.getValue();
	}
	
	@Override
	public int getByteCount() {
		return colour.getByteCount() 
				+ reserved1.getByteCount() 
				+ power.getByteCount()
				+ 32 //size of label
				+ reserved2.getByteCount();
	}

	@Override
	public String getHexData() {
		StringBuilder sb = new StringBuilder();
		sb.append(colour.getHexData());
		sb.append(reserved1.getHexData());
		sb.append(power.getHexData());
		byte[] bLabel = new byte[32];
		System.arraycopy(label.getBytes(), 0, bLabel, 0, label.getBytes().length);
		sb.append( Hex.encodeHexString(bLabel).toUpperCase());
		sb.append(reserved2.getHexData());
		logger.trace("[{}] hex data [{}]", name, sb.toString());
		return sb.toString();
	}

	@Override
	public String print() {
		JsonObjectBuilder builder = Json.createObjectBuilder()
				.add("colour", colour.print())
				.add("power", isOn())
				.add("label", label);
		String json = builder.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		colour.setFromBytes(hex.substring(0, 16));
		short sPower = new BigInteger(hex.substring(20, 24), 16).shortValue();
		power.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sPower).array());
		String sLabel = hex.substring(24, 88);
		try {
			label = new String(Hex.decodeHex(sLabel.toCharArray())).trim();
		} catch (DecoderException e) {
			String errMsg = String.format("Failed to decode hex of device label. Hex data [%1$s]", sLabel);
			logger.error("Failed to decode hex of device label [{}]", errMsg);
		}
	}
}
