package com.genie.lifx.model.payloads;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.genie.lifx.constants.PowerLevels;
import com.genie.lifx.model.LIFXPayload;
import com.genie.lifx.model.structures.UInt16;
import com.genie.lifx.model.structures.UInt32;

/**
 * Payload for setting the device power.
 * @author Matthew Roche
 *
 */
public class SetPower117 extends LIFXPayload {
	
	private UInt16 level = new UInt16(); //default is off
	private UInt32 duration = new UInt32(500); //default is 500ms
	
	public SetPower117() {
		super(117);
	}
	
	/**
	 * @param powerLevel (ON/OFF)
	 */
	public SetPower117(PowerLevels powerLevel) {
		this();
		setLevel(powerLevel);
	}
	
	/**
	 * 
	 * @param powerLevel (ON/OFF)
	 * @param duration (milliseconds)
	 */
	public SetPower117(PowerLevels powerLevel, int duration) {
		this(powerLevel);
		setDuration(duration);
	}
	
	public void setLevel(PowerLevels powerLevel) {
		byte[] data = ByteBuffer.allocate(level.getByteCount()).putShort(powerLevel.getValue()).array();
		this.level.setData(data);
	}
	
	public boolean isOn() {
		short value = ByteBuffer.wrap(level.getData()).getShort();
		return value == PowerLevels.ON.getValue();
	}
	
	/**
	 * Duration is in milliseconds
	 * @param duration
	 */
	public void setDuration(int duration) {
		byte[] data = ByteBuffer.allocate(this.duration.getByteCount()).putInt(duration).array();
		this.duration.setData(data);
	}
	
	public int getDuration() {
		return ByteBuffer.wrap(duration.getData()).getInt();
	}

	@Override
	public int getByteCount() {
		return level.getByteCount() + duration.getByteCount();
	}

	@Override
	public String getHexData() {
		StringBuilder sb = new StringBuilder();
		sb.append(level.getHexData());
		sb.append(duration.getHexData());
		logger.trace("[{}] hex data [{}]", name, sb.toString());
		return sb.toString();
	}

	@Override
	public String print() {
		JsonObjectBuilder builder = Json.createObjectBuilder()
				.add("level", isOn())
				.add("duration", getDuration());
		String json = builder.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		short sLevel = new BigInteger(hex.substring(0, 4), 16).shortValue();
		int iDuration = new BigInteger(hex.substring(4, 12), 16).intValue();
		level.setData(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(sLevel).array());
		duration.setData(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(iDuration).array());
	}

}
