package com.genie.lifx.model.payloads;

import com.genie.lifx.model.LIFXPayload;

public class EmptyPayload extends LIFXPayload {
	
	public EmptyPayload() {
		this(0);
	}
	
	public EmptyPayload(int type) {
		super(type);
	}

	public int getByteCount() {
		return 0;
	}

	public String getHexData() {
		return "";
	}

	public String print() {
		return null;
	}

}
