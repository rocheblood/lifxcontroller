package com.genie.lifx.model.payloads;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.genie.lifx.model.LIFXPayload;
import com.genie.lifx.model.structures.HSBKColor;
import com.genie.lifx.model.structures.UInt32;
import com.genie.lifx.model.structures.UInt8;

public class SetColor102 extends LIFXPayload {
	
	private UInt8 reserved = new UInt8();
	private HSBKColor colour;
	private UInt32 duration;
	
	public SetColor102() {
		//warm light by default with 500ms transition
		this(HSBKColor.WARM_WHITE, 500); 
	}
	
	public SetColor102(HSBKColor colour, int duration) {
		super(102);
		this.colour = colour;
		this.duration = new UInt32(duration);
	}
	
	public SetColor102 setColor(HSBKColor colour) {
		this.colour = colour;
		return this;
	}
	
	/**
	 * Set transition duration in ms
	 * @param duration
	 * @return
	 */
	public SetColor102 setDuration(int duration) {
		this.duration = new UInt32(duration);
		return this;
	}
	
	@Override
	public int getByteCount() {
		return reserved.getByteCount() + colour.getByteCount() + duration.getByteCount();
	}

	@Override
	public String getHexData() {
		StringBuilder sb = new StringBuilder();
		sb.append(reserved.getHexData());
		sb.append(colour.getHexData());
		sb.append(duration.getHexData());
		logger.trace("[{}] hex data [{}]", name, sb.toString());
		return sb.toString();
	}

	@Override
	public String print() {
		JsonObjectBuilder builder = Json.createObjectBuilder()
				.add("colour", colour.print())
				.add("duration", duration.getHexData());
		String json = builder.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		colour.setFromBytes(hex.substring(2, 18));
		int iDuration = Integer.parseInt(hex.substring(18, 26), 16);
		duration.setData(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(iDuration).array());
	}

}
