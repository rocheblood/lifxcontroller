package com.genie.lifx.model;

import java.io.StringReader;

import javax.json.Json;

public class LIFXHeader extends AbstractLIFXComponent {
	
	/**
	 * Contains :
	 * <ul>
	 * <li>16 bit : size</li>
	 * <li>2 bit  : origin</li>
	 * <li>1 bit  : tagged</li>
	 * <li>1 bit  : addressable</li>
	 * <li>12 bit : protocol</li>
	 * <li>32 bit : source</li>
	 * </ul>
	 */
	private LIFXFrame frame = new LIFXFrame();
	
	/**
	 * Contains :
	 * <ul>
	 * <li>64 bit : target</li>
	 * <li>48 bit : reserved</li>
	 * <li>6 bit  : reserved</li>
	 * <li>1 bit  : ack_required</li>
	 * <li>1 bit  : res_required</li>
	 * <li>8 bit  : sequence</li>
	 * </ul>
	 */
	private LIFXFrameAddress frameAddress = new LIFXFrameAddress();

	/**
	 * Contains :
	 * <ul>
	 * <li>64 bit : reserved</li>
	 * <li>16 bit : type</li>
	 * <li>16 bit : reserved</li>
	 * </ul>
	 */
	private LIFXProtocolHeader protocolHeader = new LIFXProtocolHeader();
	
	public LIFXFrame getFrame() {
		return frame;
	}

	public LIFXFrameAddress getFrameAddress() {
		return frameAddress;
	}

	public LIFXProtocolHeader getProtocolHeader() {
		return protocolHeader;
	}

	@Override
	public int getByteCount() {
		return frame.getByteCount()
				+ frameAddress.getByteCount()
				+ protocolHeader.getByteCount();
	}

	@Override
	public String getHexData() {
		String hex = frame.getHexData()
				+ frameAddress.getHexData() 
				+ protocolHeader.getHexData();
		logger.trace("[{}] hex data [{}]", name, hex);
		return hex;
	}

	@Override
	public String print() {
		String json = Json.createObjectBuilder()
				.add("frame", Json.createReader(new StringReader(frame.print())).readValue())
				.add("frameAddress", Json.createReader(new StringReader(frameAddress.print())).readValue())
				.add("protocolHeader", Json.createReader(new StringReader(protocolHeader.print())).readValue())
				.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}

	@Override
	public void setFromBytes(String hex) {
		super.setFromBytes(hex);
		int startPointer = 0;
		int endPointer = frame.getByteCount() * 2;
		frame.setFromBytes(hex.substring(startPointer, endPointer));
		startPointer = endPointer;
		endPointer += (frameAddress.getByteCount() * 2); 
		frameAddress.setFromBytes(hex.substring(startPointer, endPointer));
		startPointer = endPointer;
		endPointer += (protocolHeader.getByteCount() * 2);
		protocolHeader.setFromBytes(hex.substring(startPointer, endPointer));
	}
}
