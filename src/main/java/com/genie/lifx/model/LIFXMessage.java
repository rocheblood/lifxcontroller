package com.genie.lifx.model;

import static com.genie.lifx.constants.Properties.DEVICE_PORT;

import java.io.StringReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Random;

import javax.json.Json;
import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.constants.MessageTypes;
import com.genie.lifx.constants.Properties;
import com.genie.lifx.exception.LIFXException;
import com.genie.lifx.exception.LIFXValidationException;
import com.genie.lifx.model.payloads.EmptyPayload;
import com.genie.lifx.model.strategies.LIFXMessageStrategy;
import com.genie.lifx.model.structures.UInt32;
import com.genie.lifx.model.structures.UInt8;

/**
 * This LIFXMessage structure is compatible with LIFX message structure version v2.0
 * @author Matthew Roche
 *
 */
public class LIFXMessage extends AbstractLIFXComponent {
	
	/**
	 * Set this if the message is intended for a single device. Tagged should be 0 and target should be set alongside this field
	 */
	private InetSocketAddress ip;
	
	private LIFXHeader header = new LIFXHeader();

	private LIFXPayload payload = new EmptyPayload();
	
	/**
	 * LIFXMessage should be created through {@link LIFXMessage.Builder}
	 */
	private LIFXMessage() {}
	
	public InetSocketAddress getIP() {
		return ip;
	}
	
	public String getMAC() {
		return header.getFrameAddress().getMAC();
	}
	
	public LIFXHeader getHeader() {
		return this.header;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends LIFXPayload> T getPayload() {
		return (T) this.payload;
	}
	
	/**
	 * Returns an identifier created from the source and sequence.
	 * @return
	 */
	public String getId() {
		StringBuilder hex = new StringBuilder();
		UInt32 source = new UInt32(getSource());
		UInt8 sequence = new UInt8(getSequence());
		hex.append(source.getHexData());
		hex.append(sequence.getHexData());
		return hex.toString();
	}
	
	/**
	 * Return the source of the message. <br>
	 * Header::Frame::Source
	 * @return
	 */
	public int getSource() {
		return header.getFrame().getSource();
	}
	
	/**
	 * Return if message requires acknowledgement <br>
	 * Header::FrameAddress:AckRequired
	 * @return
	 */
	public boolean isAcknowledgementRequired() {
		return header.getFrameAddress().isAcknowledgementRequired();
	}
	
	/**
	 * Return if message requires a response <br>
	 * Header::FrameAddress:ResRequired
	 * @return
	 */
	public boolean isResRequired() {
		return header.getFrameAddress().isResponseRequired();
	}
	
	/**
	 * Return sequence <br>
	 * Header::FrameAddress::Sequence
	 * @return
	 */
	public byte getSequence() {
		return header.getFrameAddress().getSequence();
	}
	
	/**
	 * Return message type <br>
	 * Header::ProtocolHeader::Type
	 * @return
	 */
	public short getType() {
		return header.getProtocolHeader().getType();
	}
	
	@Override
	public int getByteCount() {
		return header.getByteCount() + payload.getByteCount();
	}

	@Override
	public String getHexData() {
		StringBuilder sb = new StringBuilder();
		sb.append(header.getHexData());
		sb.append(payload.getHexData());
		logger.trace("[{}] hex data [{}]", name, sb.toString());
		return sb.toString();
	}
	
	@Override
	public String print() {
		String json = Json.createObjectBuilder()
			.add("ip", ip == null ? JsonObject.NULL : Json.createValue(ip.getHostString() + ":" + ip.getPort()))
			.add("header", Json.createReader(new StringReader(header.print())).readValue())
			.add("payload", payload.print() == null ? JsonObject.NULL : Json.createReader(new StringReader(payload.print())).readValue())
			.build().toString();
		logger.trace("[{}] print [{}]", name, json);
		return json;
	}
	
	@Override
	public void setFromBytes(String hex) {
		// No need to call super here for validation. All components will validate themselves
		int startPointer = 0;
		int endPointer = header.getByteCount() * 2; //Multiple by 2 due to two hex characters per byte
		header.setFromBytes(hex.substring(startPointer, endPointer));

		payload = MessageTypes.getMessageTypesFromType(getType()).getPayload();
		startPointer = endPointer;
		endPointer += (payload.getByteCount() * 2);
		payload.setFromBytes(hex.substring(startPointer, endPointer));
	}
	
	/**
	 * Builder class for LIFXMessage objects. Contains convenience methods for setting all adjustable header
	 * params, payload and address/target.
	 * @author Matthew Roche
	 *
	 */
	public static class Builder {
		
		private Logger logger = LoggerFactory.getLogger(Builder.class);

		private LIFXMessage message;
		
		public Builder() {
			this.message = new LIFXMessage();
		}
		
		/**
		 * Used to alter an existing message or alter a template message
		 * @param message
		 */
		public Builder(LIFXMessage message) {
			this.message = message;
		}
		
		/**
		 * Creates a LIFXMessage from a hex string like when receiving a response
		 * @param hex
		 */
		public Builder(String hex) {
			this.message = new LIFXMessage();
			this.message.setFromBytes(hex);
		}
		
		public Builder(LIFXMessageStrategy strategy) {
			this.message = new LIFXMessage();
			if (strategy.getIp() != null) {
				ip(strategy.getIp());
			}
			tagged(strategy.isTagged());
			if (strategy.getSource() != null) {
				source(strategy.getSource());
			}
			if (strategy.getTarget() != null) {
				target(strategy.getTarget());
			}
			resRequired(strategy.isResRequired());
			ackRequired(strategy.isAckRequired());
			if (strategy.getSequence() != null) {
				sequence(strategy.getSequence());
			}
			if (strategy.getType() != null) {
					type(strategy.getType());
			}
			if (strategy.getPayload() != null) {
				payload(strategy.getPayload());
			}
			
		}
		
		public LIFXMessage build() {
			validate();
			LIFXMessage builtMessage = message;
			message = new LIFXMessage();
			builtMessage.header.getFrame().setSize(builtMessage.getByteCount());
			return builtMessage;
		}
		
		public Builder tagged(boolean tagged) {
			message.header.getFrame().setTagged(tagged);
			if (tagged) {
				message.header.getFrameAddress().setTarget(0);
			}
			return this;
		}
		
		public Builder source(int source) {
			message.header.getFrame().setSource(source);
			return this;
		}
		
		public Builder target(long target) {
			message.header.getFrameAddress().setTarget(target);
			message.header.getFrame().setTagged(target == 0x0);
			return this;
		}
		
		public Builder ackRequired(boolean ackRequired) {
			message.header.getFrameAddress().setAcknowledgementRequired(ackRequired);
			return this;
		}
		
		public Builder resRequired(boolean resRequired) {
			message.header.getFrameAddress().setResponseRequired(resRequired);
			return this;
		}
		
		public Builder sequence(int sequence) {
			message.header.getFrameAddress().setSequence((byte) sequence);
			return this;
		}
		
		public Builder type(int type) {
			message.header.getProtocolHeader().setType((short) type);
			return this;
		}
		
		/**
		 * Sets the payload and {@link LIFXProtocolHeader} type field
		 * @param payload
		 * @return
		 */
		public Builder payload(LIFXPayload payload) {
			message.payload = payload;
			message.header.getProtocolHeader().setType((short) payload.getType());
			return this;
		}
		
		public Builder ip(String ip) {
			return ip(ip, DEVICE_PORT.getValue());
		}
		
		/**
		 * Use this to define a specific IP for the message. 
		 * @param ip
		 * @param port
		 * @return
		 */
		public Builder ip(String ip, int port) {
			try {
				message.ip = new InetSocketAddress(InetAddress.getByName(ip), port);
				return this;
			} catch (UnknownHostException e) {
				String msg = String.format("Failed to parse address. IP [%1$s], Port [%2$s]", ip, port);
				throw new LIFXException(msg, e);
			}
		}
		
		/**
		 * Sets IP to the broadcast address (255.255.255.255)
		 * @return
		 */
		public Builder broadcast() {
			ip(Properties.BROADCAST_ADDRESS.getValue());
			return this;
		}
		
		private void validate() {
			LIFXFrame frame = message.header.getFrame();
			LIFXFrameAddress frameAddr = message.header.getFrameAddress();
			if (frame.isTagged() && frameAddr.getTarget() != 0x00L) {
				throw new LIFXValidationException("Target should be 0 when tagged is true.");
			}
			if (!frame.isTagged() && (frameAddr.getTarget() == 0x00L && message.ip == null)) {
				throw new LIFXValidationException("Target and/or IP should should be populated when tagged is false.");
			}
			if (message.ip == null) {
				logger.debug("IP is null and target set. Setting message to broadcast");
				broadcast();
			}
			if (message.header.getProtocolHeader().getType() == 0) {
				throw new LIFXValidationException("Non-zero message type (FrameAddress.type) required.");
			}
			if (!frameAddr.isSequenceSet()) {
				logger.debug("No sequence set. Setting random sequence.");
				sequence(new Random().nextInt());
			}
		}
				
	}
}
