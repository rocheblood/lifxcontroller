package com.genie.lifx.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.exception.LIFXException;
import com.genie.lifx.model.LIFXMessage;
import com.genie.lifx.util.ByteUtil;

public class LIFXClientSender implements Callable<Void> {
	
	private static Logger logger = LoggerFactory.getLogger(LIFXClientSender.class);
	
	private BlockingQueue<LIFXMessage> requestQueue;

	LIFXClientSender(BlockingQueue<LIFXMessage> requestQueue) {
		this.requestQueue = requestQueue;
	}

	@Override
	public Void call() throws Exception {
		while (true) {
			LIFXMessage msg = requestQueue.take();
			send(msg);
		}
	}
	
	private void send(LIFXMessage msg) {
		logger.info("Sending message type [{}] to IP [{}] with ID [{}]", msg.getType(), msg.getIP(), msg.getId());
		try {
			byte[] data = ByteUtil.hexToByteArray(msg.getHexData());
			DatagramSocket dsocket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket(data, data.length, msg.getIP().getAddress(),
					msg.getIP().getPort());
			dsocket.send(packet);
			dsocket.close();
		} catch (IOException e) {
			String errMsg = String.format("Failed to send packet [%1$s] to address [%2$s].",
					msg.getHexData(), 
					msg.getIP());
			logger.error(errMsg);
			throw new LIFXException(errMsg, e);
		}
	}
}
