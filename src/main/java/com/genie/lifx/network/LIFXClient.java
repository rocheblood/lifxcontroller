package com.genie.lifx.network;

import static com.genie.lifx.constants.Properties.CLEANER_FREQUENCY;
import static com.genie.lifx.constants.Properties.RESPONSE_TIMEOUT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.exception.LIFXException;
import com.genie.lifx.model.LIFXMessage;
import com.genie.lifx.network.model.LIFXClientResponse;

public class LIFXClient implements LIFXMessageListener {

	private Logger logger = LoggerFactory.getLogger(LIFXClient.class);
	
	private static final int REQUEST_QUEUE_SIZE = 20;
	
	private static LIFXClient instance;
	
	private List<LIFXMessageListener> listeners = new ArrayList<>();
	
	/**
	 * Three threads.
	 * <p>
	 * 	<ul>
	 * 		<li>{@link LIFXClientSender}</li>
	 * 		<li>{@link LIFXClientReceiver}</li>
	 * 		<li>{@link LIFXClientCleaner}</li>
	 * 	</ul>
	 * </p>
	 */
	private ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);

	/**
	 * Queue of all requests waiting to be executed.
	 */
	private BlockingQueue<LIFXMessage> requestQueue = new LinkedBlockingQueue<>(REQUEST_QUEUE_SIZE);
	
	/**
	 * List of {@link LIFXMessage} that are waiting for a response/acknowledgement.
	 * The key is the hex of the source and sequence concatenated 
	 */
	private Map<String, LIFXClientResponse> responseMap = new ConcurrentHashMap<>();
	
	private volatile Boolean open = false;
	
	/**
	 * Used to notify threads waiting on responses
	 */
	private final Object responseLock = new Object();
	
	private LIFXClient() {
	}
	
	public static LIFXClient getInstance() {
		if (instance == null) {
			instance = new LIFXClient();
		}
		return instance;
	}
	
	public void addListener(LIFXMessageListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}
	
	public void removeListener(LIFXMessageListener listener) {
		listeners.remove(listener);
	}
	
	public void open() {
		synchronized (open) {
			if (!open) {
				open = true;
				executor.submit(new LIFXClientReceiver(responseMap, responseLock, this));
				executor.submit(new LIFXClientSender(requestQueue));
				executor.scheduleAtFixedRate(new LIFXClientCleaner(responseMap), 1000L * 300, CLEANER_FREQUENCY.getValue(), TimeUnit.MINUTES);
			}
		}
	}
	
	public void close() {
		synchronized (open) {
			if (open) {
				open = false;
				executor.shutdownNow();
			}
		}
	}
	
	public boolean isOpen() {
		synchronized (open) { //Synchronize in case client is opened by a different thread
			return open;
		}
	}
	
	public void send(LIFXMessage request) {
		try {
			addToRequestQueue(request);
		} catch (InterruptedException e) {
			throw new LIFXException("Failed to add message to queue. Thread interrupted.", e);
		}
	}
	
	/**
	 * Added the message to the queue for execution. Non blocking call. If the message has a response,
	 * it will be sent to the listener
	 * @param request
	 * @throws InterruptedException 
	 */
	public void addToRequestQueue(LIFXMessage request) throws InterruptedException {
		if (!isOpen()) {
			throw new LIFXException("Client has not been opened. Please call LIFXClient.open() first.");
		}
		logger.debug("Adding request [{}] to queue. Response expected [{}]", request.getId(), request.isResRequired());
		requestQueue.put(request);
	}
	
	/**
	 * 
	 * Adds the message to the queue for execution and waits for the response/acknowledgement
	 * @param request
	 * @return LIFXClientResponse or null if no response
	 */
	public LIFXClientResponse sendForResponse(LIFXMessage request) {
		if (!request.isAcknowledgementRequired() && !request.isResRequired()) {
			throw new IllegalArgumentException("Message parameters do not expect a response or acknowledgement.");
		}
		String id = request.getId();
		try {
			addToRequestQueue(request);
			LIFXClientResponse response = null;
			
			synchronized (responseLock) {
				long startTime = System.currentTimeMillis();
				long responseTimeout = RESPONSE_TIMEOUT.getValue();
				while (System.currentTimeMillis() < (startTime + responseTimeout) && (responseMap.get(id) == null || !responseMap.get(id).isReady())) {
					logger.debug("Response not ready for request [{}]. Waiting...", id);
					responseLock.wait(1500);
				}
				response = responseMap.remove(id);
				if (response == null || response.size() == 0) {
					String errMsg = String.format("No response found for request [%1$s]", id);
					logger.warn(errMsg);
					return null;
				}
			}
			logger.info("Response ID [{}] received with [{}] messages", id, response.size());
			return response;
		} catch (InterruptedException e) {
			String errMsg = String.format("Failed to get response for request with source : [%1$s] and sequence : [%2$s]", request.getSource(), request.getSequence());
			logger.error(errMsg, e);
			throw new LIFXException(errMsg, e);
		}
	}

	@Override
	public void handleResponse(int source, int sequence, LIFXMessage response) {
		logger.debug("Listener count [{}]. Sending message [{}] to source [{}] and sequence [{}]", listeners.size(), response.getId(), source, sequence);
		for (LIFXMessageListener listener : listeners) {
			listener.handleResponse(source, sequence, response);
		}
	}
	
	
}
