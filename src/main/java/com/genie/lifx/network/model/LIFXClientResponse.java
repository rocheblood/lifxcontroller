package com.genie.lifx.network.model;

import static com.genie.lifx.constants.Properties.BROADCAST_RESPONSE_DELAY;
import static com.genie.lifx.constants.Properties.RESPONSE_QUEUE_LIFESPAN;

import java.util.ArrayList;
import java.util.List;

import com.genie.lifx.model.LIFXMessage;

public class LIFXClientResponse {
	
	private String id;
	private List<LIFXMessage> messages = new ArrayList<>();
	
	/**
	 * The time that this object was created
	 */
	private long timeReceived;
	
	private boolean broadcast = false;
	
	public LIFXClientResponse() {
		this.timeReceived = System.currentTimeMillis();
	}
	
	public LIFXClientResponse(String id) {
		this();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<LIFXMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<LIFXMessage> messages) {
		this.messages = messages;
	}
	
	public void addMessage(LIFXMessage message) {
		this.messages.add(message);
	}

	public long getTimeReceived() {
		return timeReceived;
	}
	
	/**
	 * Sets the broadcast to true. This indicates that multiple messages are expected
	 */
	public LIFXClientResponse setBroadcast() {
		this.broadcast = true;
		return this;
	}
	
	public int size() {
		return messages.size();
	}
	
	/**
	 * Returns true if time elapsed is &gt; BROADCAST_RESPONSE_DELAY
	 * If response is for a broadcast, this will be false until elapsed time.
	 * If not broadcast, this will be true if a message has been received or time has elapsed.
	 * @return
	 */
	public boolean isReady() {
		long broadcastResponseDelay = BROADCAST_RESPONSE_DELAY.getValue();
		boolean time = System.currentTimeMillis() > (timeReceived + broadcastResponseDelay );
		if (time) {
			return true;
		} else {
			return !broadcast && size() > 0;
		}
	}
	
	/**
	 * Returns true if this response is expired. That is, if 
	 * the current time is greater than the time the object was 
	 * created + LIFESPAN
	 * @return
	 */
	public boolean isExpired() {
		long responseQueueLifespan = RESPONSE_QUEUE_LIFESPAN.getValue();
		return System.currentTimeMillis() > (timeReceived + responseQueueLifespan);
	}
}
