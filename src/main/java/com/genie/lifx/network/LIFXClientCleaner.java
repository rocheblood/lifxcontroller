package com.genie.lifx.network;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.network.model.LIFXClientResponse;

public class LIFXClientCleaner implements Runnable {
	
	private static Logger logger = LoggerFactory.getLogger(LIFXClientCleaner.class);
	
	private Map<String, LIFXClientResponse> responseMap;

	public LIFXClientCleaner(Map<String, LIFXClientResponse> responseMap) {
		this.responseMap = responseMap;
	}

	@Override
	public void run() {
		int count = 0;
		for (Entry<String, LIFXClientResponse> entry : responseMap.entrySet()) {
			String key = entry.getKey();
			LIFXClientResponse response = entry.getValue();
			if (response.isExpired()) {
				count += 1;
				responseMap.remove(key);
			}
		}
		logger.info("Purged [{}] responses.", count);
		
	}

}
