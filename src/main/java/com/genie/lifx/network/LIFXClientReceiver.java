package com.genie.lifx.network;

import static com.genie.lifx.constants.Properties.DEVICE_PORT;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.exception.LIFXException;
import com.genie.lifx.model.LIFXMessage;
import com.genie.lifx.network.model.LIFXClientResponse;
import com.genie.lifx.util.ByteUtil;

public class LIFXClientReceiver implements Callable<Void> {

	private static Logger logger = LoggerFactory.getLogger(LIFXClientReceiver.class);
	
	private LIFXMessageListener listener;
	private Map<String, LIFXClientResponse> responseMap;
	private Object responseLock;
	private DatagramChannel dChannel;

	LIFXClientReceiver(Map<String, LIFXClientResponse> responseMap, Object responseLock, LIFXMessageListener listener) {
		this.responseMap = responseMap;
		this.responseLock = responseLock;
		this.listener = listener;
		try {
			dChannel = DatagramChannel.open();
			dChannel.bind(new InetSocketAddress(DEVICE_PORT.getValue()));
		} catch (IOException e) {
			String errMsg = String.format("Failed to create DatagramSocket on port [%1$s]", DEVICE_PORT.getValue());
			logger.error(errMsg);
			throw new LIFXException(errMsg, e);
		}
	}

	/**
	 * Check network 
	 * Find responses and parse into LIFXMessages 
	 * sync on responesLock
	 * add to responseMap 
	 * notifyAll so that some thread will be aware that it's response has been received.
	 */
	@Override
	public Void call() throws Exception {
		ByteBuffer buffer = ByteBuffer.allocate(1024);

		while (true) {
			logger.debug("ClientReceiver waiting for a message");
			SocketAddress addr = dChannel.receive(buffer);
			
			String hex = ByteUtil.getHex(buffer.array());
			buffer.clear();
			
			String hostAddr = ((InetSocketAddress) addr).getAddress().getHostAddress();
			
			LIFXMessage responseMessage = new LIFXMessage.Builder(hex)
					.ip(hostAddr)
					.build();
			
			/*
			 * If a request is broadcast, then this receiver will detect it. Therefore don't handle
			 * messages that originated from this IP. 
			 */
			if (hostAddr.equals(InetAddress.getLocalHost().getHostAddress())) {
				logger.debug("Broadcast request detected. Creating broadcast response...");
				LIFXClientResponse response = new LIFXClientResponse(responseMessage.getId());
				response.setBroadcast();
				responseMap.put(response.getId(), response);
				continue;
			}
			
			logger.debug("Received message with type [{}] and id [{}] from [{}] with contents [{}].", 
					responseMessage.getType(),
					responseMessage.getId(),
					hostAddr, 
					hex);
			logger.debug(responseMessage.print());
			
			listener.handleResponse(responseMessage.getSource(), responseMessage.getSequence(), responseMessage);
			
			store(responseMessage);
		}
	}
	
	/**
	 * Add response to responseMap for consumer to retrieve
	 * @param responseMessage
	 */
	public void store(LIFXMessage responseMessage) {
		synchronized(responseLock) {
			if (responseMap.containsKey(responseMessage.getId())) {
				responseMap.get(responseMessage.getId()).addMessage(responseMessage);
			} else {
				LIFXClientResponse response = new LIFXClientResponse(responseMessage.getId());
				response.addMessage(responseMessage);
				responseMap.put(response.getId(), response);
			}
			responseLock.notifyAll();
		}
	}

}
