package com.genie.lifx.network;

import com.genie.lifx.model.LIFXMessage;

public interface LIFXMessageListener {

	/**
	 * 
	 * @param source Client who sent the message
	 * @param sequence Message id
	 * @param response
	 */
	public void handleResponse(int source, int sequence, LIFXMessage response);
}
