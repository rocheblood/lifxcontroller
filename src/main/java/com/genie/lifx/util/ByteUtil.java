package com.genie.lifx.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.genie.lifx.exception.LIFXException;

public class ByteUtil {

	public static String getHex(byte[] data) {
		return getHex(ByteBuffer.wrap(data));
	}
	
	public static String getHex(short data, ByteOrder order) {
		ByteBuffer buffer = ByteBuffer.allocate(2).order(order).putShort(data);
		return getHex(buffer);
	}
	
	public static String getHex(int data, ByteOrder order) {
		ByteBuffer buffer = ByteBuffer.allocate(4).order(order).putInt(data);
		return getHex(buffer);
	}
	
	public static String getHex(long data, ByteOrder order) {
		ByteBuffer buffer = ByteBuffer.allocate(8).order(order).putLong(data);
		return getHex(buffer);
	}
	
	public static String getHex(ByteBuffer buffer) {
		final StringBuilder builder = new StringBuilder();
	    for(byte b : buffer.array()) {
	        builder.append(String.format("%02x", b));
	    }
	    return builder.toString().toUpperCase();
	}
	
	/**
	 * Returns a String representing the byte as binary
	 * @param value
	 * @return
	 */
	public static String byteToBinary(byte value) {
		return String.format("%8s", Integer.toBinaryString(value & 0xFF)).replace(' ', '0');
	}
	
	public static String byteToBinary(byte[] data) {
		StringBuilder sb = new StringBuilder();
		for (byte b : data) {
			sb.append(byteToBinary(b));
		}
		return sb.toString();
	}
	
	/**
	 * Converts an int to a byte array of length 4
	 * @param value
	 * @return
	 */
	public static byte[] intToByte(int value) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.putInt(value);
		return buffer.array();
	}
	
	/**
	 * Converts a short to a byte array of length 2
	 * @param value
	 * @return
	 */
	public static byte[] shortToByte(short value) {
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.putShort(value);
		return buffer.array();	
	}
	
	public static byte[] hexToByteArray(String hex) {
		try {
			return Hex.decodeHex(hex.toCharArray());
		} catch (DecoderException e) {
			throw new LIFXException("Failed to decode hex to byte array with hex [%1$s]", e, hex);
		}
	}
}
