package com.genie.lifx.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesManager {
	
	private static Logger logger = LoggerFactory.getLogger(PropertiesManager.class);
	
	/**
	 * Interface to abstract the conversion of a property object to its correct type.
	 * @author Matt
	 *
	 * @param <T>
	 */
	public static interface PropertyConverter<T> {
		public T get(Object value);
	}
	
	private static final PropertyConverter<Integer> INT_CONVERTER = 
			(value) -> {
				return Integer.parseInt(value.toString());
			};
	private static final PropertyConverter<Long> LONG_CONVERTER = 
			(value) -> {
				return Long.parseLong(value.toString());
			};
	private static final PropertyConverter<String> STRING_CONVERTER = 
			(value) -> {return value.toString();};
	
	private final Map<Class<?>, PropertyConverter<?>> converters = new HashMap<>();

	//TODO make configurable
	public static final String DEFAULT_PROPERTIES_NAME = "genie.properties";
	
	private static PropertiesManager instance = null;
	
	private Properties props = new Properties();
	
	/**
	 * Returns an instance of PropertiesManager which uses the default (genie.properties) file.
	 * @return
	 */
	public static synchronized PropertiesManager getInstance() {
		if (instance == null) {
			instance = new PropertiesManager(DEFAULT_PROPERTIES_NAME);
		}
		return instance;
	}
	
	private PropertiesManager(String fileName) {
		converters.put(Integer.class, INT_CONVERTER);
		converters.put(Long.class, LONG_CONVERTER);
		converters.put(String.class, STRING_CONVERTER);
		
		try {
			InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName);
			if (stream != null) {
				logger.info("Loading properties file from resources [{}]", fileName);
				props.load(stream);
			} else {
				logger.info("Cannot find file [{}] in resources. Default values will be used.", fileName);
			}
		} catch (IOException e) {
			String errMsg = String.format("Failed to load properties file [%1$s]. Default values will be used.", fileName);
			logger.warn(errMsg);
		}
	}
	
	public Object get(String key, Object defaultValue) {
		return props.getOrDefault(key, defaultValue);
	}
	
	public PropertyConverter<?> getPropertyConverter(Class<?> type) {
		return converters.get(type);
		
	}
}
