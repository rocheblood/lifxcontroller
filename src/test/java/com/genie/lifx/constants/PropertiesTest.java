package com.genie.lifx.constants;

import static com.genie.lifx.constants.Properties.BROADCAST_ADDRESS;
import static com.genie.lifx.constants.Properties.BROADCAST_RESPONSE_DELAY;
import static com.genie.lifx.constants.Properties.CLEANER_FREQUENCY;
import static com.genie.lifx.constants.Properties.DEVICE_PORT;
import static com.genie.lifx.constants.Properties.RESPONSE_QUEUE_LIFESPAN;
import static com.genie.lifx.constants.Properties.RESPONSE_TIMEOUT;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class PropertiesTest {

	@Test
	public void getValueUsingDefaultsCastTest() {
		Long responseTimeout = RESPONSE_TIMEOUT.getDefaultValue();
		Long responseQueueLifespan = RESPONSE_QUEUE_LIFESPAN.getDefaultValue();
		Integer devicePort = DEVICE_PORT.getDefaultValue();
		String broadcastAddress = BROADCAST_ADDRESS.getDefaultValue();
		Long broadcastResponseDelay = BROADCAST_RESPONSE_DELAY.getDefaultValue();
		Long cleanerFrequency = CLEANER_FREQUENCY.getDefaultValue();
		
		assertThat(responseTimeout, is(5000L));
		assertThat(responseQueueLifespan, is(300000L));
		assertThat(devicePort, is(56700));
		assertThat(broadcastAddress, is("255.255.255.255"));
		assertThat(broadcastResponseDelay, is(500L));
		assertThat(cleanerFrequency, is(10L));
	}

	@Test
	public void getValueUsingStringsCastTest() {
		Long responseTimeout = RESPONSE_TIMEOUT.getValue();
		Long responseQueueLifespan = RESPONSE_QUEUE_LIFESPAN.getValue();
		Integer devicePort = DEVICE_PORT.getValue();
		String broadcastAddress = BROADCAST_ADDRESS.getValue();
		Long broadcastResponseDelay = BROADCAST_RESPONSE_DELAY.getValue();
		Long cleanerFrequency = CLEANER_FREQUENCY.getValue();
		
		assertThat(responseTimeout, is(500L));
		assertThat(responseQueueLifespan, is(30000L));
		assertThat(devicePort, is(5670));
		assertThat(broadcastAddress, is("255.255.255.25"));
		assertThat(broadcastResponseDelay, is(50L));
		assertThat(cleanerFrequency, is(1L));
	}

}
