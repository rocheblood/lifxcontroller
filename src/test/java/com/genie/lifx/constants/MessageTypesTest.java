package com.genie.lifx.constants;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class MessageTypesTest {

	@Test
	public void getMessageTypesFromType() {
		MessageTypes get101 = MessageTypes.getMessageTypesFromType(101);
		Assert.assertThat(get101, Matchers.is(MessageTypes.GET_101));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getMessageTypesFromType_Missing() {
		MessageTypes.getMessageTypesFromType(0);
	}

}
