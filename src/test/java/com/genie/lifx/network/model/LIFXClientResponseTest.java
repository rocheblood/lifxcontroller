package com.genie.lifx.network.model;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;

import com.genie.lifx.constants.MessageTypes;

public class LIFXClientResponseTest {

	/**
	 * GIVEN : {@link LIFXClientResponse} in non broadcast
	 * WHEN : Creating the object
	 * THEN : It should not be ready
	 */
	@Test
	public void nonBroadcastIsNotReadyWhenCreated() {
		LIFXClientResponse resp = new LIFXClientResponse();
		Assert.assertThat(resp.isReady(), is(false));
	}
	
	/**
	 * GIVEN : {@link LIFXClientResponse} in non broadcast
	 * WHEN : Adding a message
	 * THEN : It should be ready
	 */
	@Test
	public void nonBroadcastIsReadyWhenMessageAdded() {
		LIFXClientResponse resp = new LIFXClientResponse();
		resp.addMessage(MessageTypes.GET_101.getMessage());
		Assert.assertThat(resp.isReady(), is(true));
	}

	/**
	 * GIVEN : {@link LIFXClientResponse} in non broadcast
	 * WHEN : Waiting
	 * THEN : It should be ready after short period
	 * @throws InterruptedException 
	 */
	@Test
	public void nonBroadcastIsReadyAfterWaiting() throws InterruptedException {
		LIFXClientResponse resp = new LIFXClientResponse();
		Thread.sleep(800);
		Assert.assertThat(resp.isReady(), is(true));
	}

	/**
	 * GIVEN : {@link LIFXClientResponse} in broadcast
	 * WHEN : Creating
	 * THEN : It should not be ready
	 */
	@Test
	public void broadcastIsNotReadyWhenCreated() {
		LIFXClientResponse resp = new LIFXClientResponse();
		resp.setBroadcast();
		Assert.assertThat(resp.isReady(), is(false));
	}

	/**
	 * GIVEN : {@link LIFXClientResponse} in broadcast
	 * WHEN : Adding a message
	 * THEN : It should not be ready
	 */
	@Test
	public void broadcastIsNotReadyWithMessageAdded() {
		LIFXClientResponse resp = new LIFXClientResponse();
		resp.setBroadcast();
		resp.addMessage(MessageTypes.GET_101.getMessage());
		Assert.assertThat(resp.isReady(), is(false));
	}

	/**
	 * GIVEN : {@link LIFXClientResponse} in non broadcast
	 * WHEN : Waiting
	 * THEN : It should be ready
	 * @throws InterruptedException 
	 */
	@Test
	public void broadcastIsReadyAfterWaiting() throws InterruptedException {
		LIFXClientResponse resp = new LIFXClientResponse();
		resp.setBroadcast();
		resp.addMessage(MessageTypes.GET_101.getMessage());
		Thread.sleep(800);
		Assert.assertThat(resp.isReady(), is(true));
	}

}
