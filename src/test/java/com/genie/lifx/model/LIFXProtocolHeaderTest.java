package com.genie.lifx.model;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LIFXProtocolHeaderTest {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Test
	public void verifyByteAssembly() {
		LIFXProtocolHeader proto = new LIFXProtocolHeader();
		proto.setType((short) 0x66);
		Assert.assertEquals("000000000000000066000000", proto.getHexData());
	}
	
	/**
	 * GIVEN : Hex string
	 * WHEN : Create LIFXProtocolHeader from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes() {
		String hex = "000000000000000066000000";
		LIFXProtocolHeader proto = new LIFXProtocolHeader();
		proto.setFromBytes(hex);
		Assert.assertEquals(hex, proto.getHexData());
		Assert.assertEquals(proto.getType(), 102); //102 (dec), 66 (hex)
	}

}
