package com.genie.lifx.model;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;

import com.genie.lifx.constants.MessageTypes;

public class LIFXHeaderTest {

	/**
	 * GIVEN : Hex string
	 * WHEN : Create LIFXHeader from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes_Success() {
		String hex = "2A00001400000000000000000000000000000000000000C8000000000000000075000000";
		LIFXHeader header = new LIFXHeader();
		header.setFromBytes(hex);
		Assert.assertThat(header.getHexData(), is(hex));
		Assert.assertFalse(header.getFrame().isTagged());
		Assert.assertFalse(header.getFrameAddress().isAcknowledgementRequired());
		Assert.assertFalse(header.getFrameAddress().isResponseRequired());
		Assert.assertTrue(header.getFrameAddress().isSequenceSet());
		Assert.assertEquals(header.getProtocolHeader().getType(), MessageTypes.SET_POWER_117.getType());
	}

}
