package com.genie.lifx.model.payloads;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.constants.PowerLevels;
import com.genie.lifx.model.LIFXMessage;
import com.genie.lifx.model.structures.HSBKColor;

/**
 * Contains basic unit tests for each different payload
 * @author Matthew Roche
 *
 */
public class PayloadTest {
	
	private Logger logger = LoggerFactory.getLogger(PayloadTest.class);
	
	private String ip = "192.168.1.242";
	private int port = 56700;

	/**
	 * GIVEN : An packet that turns a light on
	 * WHEN : Building a new LIFXMessage
	 * THEN : Message with SetColor117 payload should match the given packet
	 */
	@Test
	public void setPower117Test() {
		SetPower117 payload = new SetPower117(PowerLevels.ON);
		LIFXMessage msg = new LIFXMessage.Builder()
				.ackRequired(false)
				.resRequired(false)
				.payload(payload) //sets type
				.sequence(1)
				.ip(ip, port)
				.build();
		logger.debug(msg.getHexData());
		String expected = "2A0000140000000000000000000000000000000000000001000000000000000075000000FFFFF4010000";
		verifyMsg(msg, expected);
	}
	
	/**
	 * GIVEN : An packet that turns a light green
	 * WHEN : Building a new LIFXMessage
	 * THEN : Message with SetColor102 payload should match the given packet
	 */
	@Test
	public void setColor102Test() {
		HSBKColor colour = new HSBKColor(120, 1, 1, 3500);
		SetColor102 payload = new SetColor102(colour, 1024);
		LIFXMessage msg = new LIFXMessage.Builder()
				.ackRequired(false)
				.resRequired(false)
				.payload(payload)
				.sequence(1)
				.ip(ip, port)
				.build();
		logger.debug(msg.getHexData());
		String expected = "310000140000000000000000000000000000000000000001000000000000000066000000005555FFFFFFFFAC0D00040000";
		verifyMsg(msg, expected);
	}
	
	/**
	 * Verifies that the expected hex is generated and that the human readable print function works
	 * @param msg
	 * @param expectedHex
	 */
	public void verifyMsg(LIFXMessage msg, String expectedHex) {
		String json = msg.print();
		assertThat(msg.getHexData(), is(expectedHex));
		assertThat(json, hasJsonPath("$.header"));
		assertThat(json, hasJsonPath("$.header.frame"));
		assertThat(json, hasJsonPath("$.header.frameAddress"));
		assertThat(json, hasJsonPath("$.header.protocolHeader"));
		assertThat(json, hasJsonPath("$.payload"));
		assertThat(json, hasJsonPath("$.ip"));
	}
}
