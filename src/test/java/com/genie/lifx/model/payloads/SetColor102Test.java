package com.genie.lifx.model.payloads;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;

public class SetColor102Test {

	/**
	 * GIVEN : Hex string
	 * WHEN : Create SetColor102 from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes_Success() {
		String hex = "005555FFFFFFFFAC0D00040000";
		SetColor102 colour = new SetColor102();
		colour.setFromBytes(hex);
		Assert.assertThat(colour.getHexData(), is(hex));
	}

}
