package com.genie.lifx.model.payloads;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;

public class SetPower117Test {

	/**
	 * GIVEN : Hex string
	 * WHEN : Create SetPower117 from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes_Success() {
		String hex = "FFFF1F400000";
		SetPower117 power = new SetPower117();
		power.setFromBytes(hex);
		Assert.assertThat(power.getHexData(), is(hex));
	}

}
