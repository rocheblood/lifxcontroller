package com.genie.lifx.model.payloads;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;

public class State107Test {
	
	/**
	 * GIVEN : Hex string
	 * WHEN : Create State107 from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes_Success() {
		String hex = "5555FFFFFFFFAC0D0000FFFF426564726F6F6D000000000000000000000000000000000000000000000000000000000000000000";
		State107 state = new State107();
		state.setFromBytes(hex);
		Assert.assertThat(state.getHexData(), is(hex));
	}
	
	@Test
	public void verifyLabel() {
		String hex = "5555FFFFFFFFAC0D0000FFFF426564726F6F6D000000000000000000000000000000000000000000000000000000000000000000";
		State107 state = new State107();
		state.setFromBytes(hex);
		Assert.assertEquals("Bedroom", state.getLabel());
		Assert.assertTrue(state.isOn());
	}

}
