package com.genie.lifx.model;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;

public class LIFXFrameAddressTest {

	@Test
	public void verifyByteAssembly() {
		LIFXFrameAddress frame = new LIFXFrameAddress();
		Assert.assertEquals("00000000000000000000000000000100", frame.getHexData());
		frame.setResponseRequired(false);
		Assert.assertEquals("00000000000000000000000000000000", frame.getHexData());
		frame.setAcknowledgementRequired(true);
		frame.setResponseRequired(true);
		frame.setTarget(0xAABBCCDDEEFFL);
		Assert.assertEquals("AABBCCDDEEFF00000000000000000300", frame.getHexData());
	}
	
	/**
	 * GIVEN : Hex string
	 * WHEN : Create LIFXFrameAddress from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes() {
		String hex = "AABBCCDDEEFF00000000000000000300";
		LIFXFrameAddress frame = new LIFXFrameAddress();
		frame.setFromBytes(hex);
		Assert.assertEquals(hex, frame.getHexData());
		Assert.assertEquals(frame.getTarget(), new BigInteger("AABBCCDDEEFF0000", 16).longValue());
		Assert.assertTrue(frame.isResponseRequired());
		Assert.assertTrue(frame.isAcknowledgementRequired());
	}

}
