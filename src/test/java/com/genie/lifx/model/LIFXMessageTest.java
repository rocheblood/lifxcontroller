package com.genie.lifx.model;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genie.lifx.constants.MessageTypes;
import com.genie.lifx.exception.LIFXValidationException;

public class LIFXMessageTest {
	
	private Logger logger = LoggerFactory.getLogger(LIFXMessageTest.class);

	/**
	 * GIVEN : A LIFXMessage Builder
	 * WHEN : Building a LIFXMessage without setting any params
	 * THEN : Expect validation error
	 */
	@Test(expected = LIFXValidationException.class)
	public void builderBaseMessage() {
		new LIFXMessage.Builder().build();
	}
	
	@Test
	public void baseMessageTagged() {
		LIFXMessage msg = new LIFXMessage.Builder()
				.tagged(true)
				.type(1)
				.ip("255.255.255.255")
				.sequence(1)
				.build();
		Assert.assertEquals(36, msg.getByteCount());
		Assert.assertThat(msg.getHexData(), is("240000340000000000000000000000000000000000000101000000000000000001000000"));
	}
	
	@Test
	public void printTest() {
		LIFXMessage msg = new LIFXMessage.Builder()
				.tagged(true)
				.type(1)
				.build();
		
		logger.debug(msg.print());
		Assert.assertThat(msg.print(), hasJsonPath("$.header"));
		Assert.assertThat(msg.print(), hasJsonPath("$.header.frame"));
		Assert.assertThat(msg.print(), hasJsonPath("$.header.frameAddress"));
		Assert.assertThat(msg.print(), hasJsonPath("$.header.protocolHeader"));
		Assert.assertThat(msg.print(), hasJsonPath("$.payload"));
		Assert.assertThat(msg.print(), hasJsonPath("$.ip"));
	}
	
	/**
	 * GIVEN : Hex string
	 * WHEN : Create LIFXMessage from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes() {
		//This hex uses the SET_POWER_117 payload
		String hex = "2A0000140000000000000000000000000000000000000052000000000000000075000000FFFFF4010000";
		LIFXMessage msg = new LIFXMessage.Builder(hex)
				.ip("127.0.0.1")
				.build();
		Assert.assertEquals(hex, msg.getHexData());
		Assert.assertFalse(msg.isAcknowledgementRequired());
		Assert.assertFalse(msg.isResRequired());
		Assert.assertEquals(MessageTypes.SET_POWER_117.getType(), msg.getType());
		Assert.assertEquals(0, msg.getSource());
		Assert.assertEquals(82, msg.getSequence());
	}

}
