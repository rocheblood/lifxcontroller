package com.genie.lifx.model.structures;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;

public class HSBKColourTest {
	
	/**
	 * GIVEN : Hex string
	 * WHEN : Create HSBKColour from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes_Success() {
		String hex = "5555FFFFFFFFAC0D";
		HSBKColor colour = HSBKColor.NONE;
		colour.setFromBytes(hex);
		Assert.assertThat(colour.getHexData(), is(hex));
	}

}
