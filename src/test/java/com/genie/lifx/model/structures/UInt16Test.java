package com.genie.lifx.model.structures;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;

import org.junit.Assert;
import org.junit.Test;

public class UInt16Test {
	
	/**
	 * GIVEN : UInt16 object
	 * WHEN : Passing in extremes
	 * THEN : Verify expected hex
	 */
	@Test
	public void testUInt16Limits() {
		int max = 65535;
		UInt16 uInt1 = new UInt16(max + 1);
		UInt16 uInt2 = new UInt16(max);
		UInt16 uInt3 = new UInt16(max - 1);
		Assert.assertThat("0000", equalTo(uInt1.getHexData()));
		Assert.assertThat("FFFF", equalToIgnoringCase(uInt2.getHexData()));
		Assert.assertThat("FEFF", equalToIgnoringCase(uInt3.getHexData()));
	}

}
