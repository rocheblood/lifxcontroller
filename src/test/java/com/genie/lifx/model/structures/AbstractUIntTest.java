package com.genie.lifx.model.structures;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.junit.Assert;
import org.junit.Test;

import com.genie.lifx.util.ByteUtil;

public class AbstractUIntTest {

	/**
	 * GIVEN : a UInt object
	 * WHEN : Setting a bit
	 * THEN : Update that bit to reflect the value at the correct position/byte
	 */
	@Test
	public void setBitAtTest() {
		short val = 2184; //0x0000 1000 1000 1000
		TestUInt uint = new TestUInt(ByteBuffer.allocate(2).putShort(val).array());
		Assert.assertEquals("0888", uint.getHexData());
		
		uint.setBitAt(0, 1);
		Assert.assertEquals("8888", uint.getHexData());
		uint.setBitAt(0, 0);
		Assert.assertEquals("0888", uint.getHexData());
		
		uint.setBitAt(4, 0);
		Assert.assertEquals("0088", uint.getHexData());
		uint.setBitAt(4, 1);
		Assert.assertEquals("0888", uint.getHexData());
		
		uint.setBitAt(5, 1);
		Assert.assertEquals("0C88", uint.getHexData());
		uint.setBitAt(5, 0);
		Assert.assertEquals("0888", uint.getHexData());
	}
	
	/**
	 * GIVEN : A UInt object
	 * WHEN : Getting a bit value at position
	 * THEN : Return the expected bit value
	 */
	@Test
	public void getBitAtPosTest() {
		short val = (short) 0xAAAA; // 0x1010 1010 1010 1010
		TestUInt uint = new TestUInt(ByteBuffer.allocate(2).putShort(val).array());
		byte valAt0 = uint.getBitAt(0);
		Assert.assertEquals(0x01, valAt0);
		
		byte valAt1 = uint.getBitAt(1);
		Assert.assertEquals(0x00, valAt1);
		
		byte valAt2 = uint.getBitAt(2);
		Assert.assertEquals(0x01, valAt2);
		
		uint.setBitAt(2, 0);
		byte newValAt2 = uint.getBitAt(2);
		Assert.assertEquals(0x00, newValAt2);
	}
	
	public static class TestUInt extends AbstractUInt {
		
		public TestUInt(byte[] data) {
			this.data = data;
		}

		public int getByteCount() {
			return 4;
		}

		public String getHexData(ByteOrder order) {
			return ByteUtil.getHex(data);
		}
	}

}
