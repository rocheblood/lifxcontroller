package com.genie.lifx.model;

import org.junit.Assert;
import org.junit.Test;

public class LIFXFrameTest {

	/**
	 * GIVEN : LIFXFrame object
	 * WHEN : Creating and resetting tagged value
	 * THEN : Expect correct hex sequence
	 */
	@Test
	public void verifyByteAssembly() {
		LIFXFrame frame = new LIFXFrame();
		Assert.assertEquals("0000001400000000", frame.getHexData());
		frame.setTagged(true);
		Assert.assertEquals("0000003400000000", frame.getHexData());
		frame.setTagged(false);
		Assert.assertEquals("0000001400000000", frame.getHexData());
	}
	
	/**
	 * GIVEN : Hex string
	 * WHEN : Create LIFXFrame from hex 
	 * THEN : Generated hex and fields should match originals
	 */
	@Test
	public void verifySetFromBytes() {
		String hex = "0000003400000000";
		LIFXFrame frame = new LIFXFrame();
		frame.setFromBytes(hex);
		Assert.assertEquals(hex, frame.getHexData());
		Assert.assertTrue(frame.isTagged());
	}

}
