package com.genie.lifx.util;

import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

import java.nio.ByteOrder;

import org.junit.Test;

public class ByteUtilTest {

	@Test
	public void getHexTest() {
		byte[] t1 = new byte[] {15};
		assertThat(ByteUtil.getHex(t1), equalToIgnoringCase("0F"));
		byte[] t2 = new byte[] {15, 0};
		assertThat(ByteUtil.getHex(t2), equalToIgnoringCase("0F00"));
		short t3 = 0x0F00; // same as byte[] {15, 0}
		assertThat(ByteUtil.getHex(t3, ByteOrder.LITTLE_ENDIAN), equalToIgnoringCase("000F"));
		assertThat(ByteUtil.getHex(t3, ByteOrder.BIG_ENDIAN), equalToIgnoringCase("0F00"));
		int t4 = 0xF0000000;
		assertThat(ByteUtil.getHex(t4, ByteOrder.LITTLE_ENDIAN), equalToIgnoringCase("000000F0"));
		assertThat(ByteUtil.getHex(t4, ByteOrder.BIG_ENDIAN), equalToIgnoringCase("F0000000"));
		long t5 = 0xF000000000000000L;
		assertThat(ByteUtil.getHex(t5, ByteOrder.LITTLE_ENDIAN), equalToIgnoringCase("00000000000000F0"));
		assertThat(ByteUtil.getHex(t5, ByteOrder.BIG_ENDIAN), equalToIgnoringCase("F000000000000000"));
	}

}
