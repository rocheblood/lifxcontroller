package com.genie.lifx.util;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class PropertiesManagerTest {

	@Test
	public void getString() {
		PropertiesManager pm = PropertiesManager.getInstance();
		Object value = pm.get("com.genie.prop1", "failed");
		Assert.assertThat(value.toString(), Matchers.is("genieProp1"));
	}

}
