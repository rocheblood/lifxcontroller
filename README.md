# Project

This project is a JAVA implementation of the [LIFX LAN protocol](https://lan.developer.lifx.com/). It allows a user to control LIFX lightbulbs in a LAN environment.

Not all message types defined in the documentation have been implemented. Currently, there is only support for :-

* [GET_101](https://lan.developer.lifx.com/docs/light-messages#section-get-101)
* [SET_COLOR_102](https://lan.developer.lifx.com/docs/light-messages#section-setcolor-102)
* [SET_POWER_117](https://lan.developer.lifx.com/docs/light-messages#section-setpower-117)

## Usage

For more examples, see LIFXClientExamples.java. 

Obtain a client to send and receive messages.

	LIFXClient client = LIFXClient.getInstance();
	try {
		client.open();
		//Send messages here
	} finally {
		client.close();
	}
	
Change colour to a predefined colour (WARM WHITE) of a bulb at IP 192.168.1.13

	SetColor102 payload = MessageTypes.SET_COLOR_102
		.getPayload()
		.setColor(HSBKColor.WARM_WHITE);
	LIFXMessage msg = MessageTypes.SET_COLOR_102.getBuilder(payload)
		.ip(192.168.1.13)
		.build();
	client.send(msg);
	
Turn bulb all bulbs off

	SetPower117 payload = MessageTypes.SET_POWER_117
		.getPayload();
		.setLevel(PowerLevels.OFF);
	LIFXMessage msg = MessageTypes.SET_POWER_117.getBuilder(payload)
				.build();
	client.send(msg);

## Future Improvements

* Complete implementations for other message types
* Light bulb onboarding
* Light bulb discovery
* Implement java.io.Closable in LIFXClient.java to allow try-with-resources
